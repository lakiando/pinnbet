//
//  AppDelegate.swift
//  PinnBet
//
//  Created by Lazar Andonov on 26/12/2019.
//  Copyright © 2019 Lazar Andonov. All rights reserved.
//

import UIKit
import LGSideMenuController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigationBar = UINavigationBar.appearance()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)

        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }

        // Override point for customization after application launch.
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationBar.tintColor = UIColor.white
        navigationBar.barTintColor = UIColor.clear

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let startViewController = storyboard.instantiateViewController(withIdentifier: "StartViewController") as? StartViewController else { return false }

        let navigation = UINavigationController(rootViewController: startViewController)

        guard let sideMenuVC = storyboard.instantiateViewController(
            withIdentifier: "SideMenuViewController") as? SideMenuViewController else { return false }

        let rootVC = LGSideMenuController(rootViewController: navigation, leftViewController: sideMenuVC, rightViewController: nil)


        window?.rootViewController = rootVC
        window?.makeKeyAndVisible()

        return true
    }



}


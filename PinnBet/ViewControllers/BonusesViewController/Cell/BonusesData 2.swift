//
//  BonusesData.swift
//  PinnBet
//
//  Created by Lazar Andonov on 28/12/2019.
//  Copyright © 2019 Lazar Andonov. All rights reserved.
//

import Foundation

struct BonusesData {
    var number: String
    var precentage: String

    init(number: String, percentage: String) {
        self.number = number
        self.precentage = percentage
    }
}

//
//  BonusCollectionViewCell.swift
//  PinnBet
//
//  Created by Lazar Andonov on 28/12/2019.
//  Copyright © 2019 Lazar Andonov. All rights reserved.
//

import UIKit

class BonusCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!

}

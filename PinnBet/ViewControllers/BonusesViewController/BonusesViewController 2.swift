//
//  BonusesViewController.swift
//  PinnBet
//
//  Created by Lazar Andonov on 28/12/2019.
//  Copyright © 2019 Lazar Andonov. All rights reserved.
//

import UIKit

class BonusesViewController: UIViewController {

    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var bonusesDescriptionLabel: UILabel!
    @IBOutlet weak var bonusesSubDescriptionLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!

    var dataSource: [BonusesData] = [
        BonusesData(number: "5", percentage: "3%"),
        BonusesData(number: "6", percentage: "5%"),
        BonusesData(number: "7", percentage: "10%"),
        BonusesData(number: "8", percentage: "15%"),
        BonusesData(number: "9", percentage: "20%"),
        BonusesData(number: "10", percentage: "25%"),
        BonusesData(number: "11", percentage: "30%"),
        BonusesData(number: "12", percentage: "35%"),
        BonusesData(number: "13", percentage: "40%"),
        BonusesData(number: "14", percentage: "45%"),
        BonusesData(number: "15", percentage: "50%"),
        BonusesData(number: "16", percentage: "55%"),
        BonusesData(number: "17", percentage: "60%"),
        BonusesData(number: "18", percentage: "65%"),
        BonusesData(number: "19", percentage: "70%"),
        BonusesData(number: "20", percentage: "75%"),
        BonusesData(number: "21", percentage: "80%"),
        BonusesData(number: "22", percentage: "90%"),
        BonusesData(number: "23", percentage: "100%"),
        BonusesData(number: "24", percentage: "110%"),
        BonusesData(number: "25", percentage: "120%"),
        BonusesData(number: "26", percentage: "140%"),
        BonusesData(number: "27", percentage: "150%"),
        BonusesData(number: "28", percentage: "160%"),
        BonusesData(number: "29", percentage: "170%"),
        BonusesData(number: "30", percentage: "180%"),
        BonusesData(number: "31", percentage: "200%"),
        BonusesData(number: "32", percentage: "210%"),
        BonusesData(number: "33", percentage: "220%"),
        BonusesData(number: "34", percentage: "230%"),
        BonusesData(number: "35", percentage: "250%")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: false)
    }

}

extension BonusesViewController {
    func setupUI() {
        navigationController?.navigationBar.topItem?.title = "Bonusi"
        navigationController?.navigationBar.barTintColor = UIColor.clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "BonusCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BonusCollectionViewCell")
        bonusesDescriptionLabel.text = Constans.bonusiTitle
        bonusesSubDescriptionLabel.text = Constans.bonusiPravila
    }
}

extension BonusesViewController: UICollectionViewDelegate {

}

extension BonusesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BonusCollectionViewCell", for: indexPath) as? BonusCollectionViewCell else { return UICollectionViewCell() }
        cell.titleLabel.text = dataSource[indexPath.row].number
        cell.percentageLabel.text = dataSource[indexPath.row].precentage
        return cell
    }
}

extension BonusesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionViewLayout as! GridLayout

        let availableWidth = collectionView.bounds.size.width
        let columns = 3
        var itemTotalWidth = availableWidth - CGFloat(columns) * layout.minimumInteritemSpacing
        itemTotalWidth -= (layout.sectionInset.left + layout.sectionInset.right)

        let itemWidth = itemTotalWidth / CGFloat(columns)
        return CGSize(width: itemWidth, height: itemWidth)
    }
}

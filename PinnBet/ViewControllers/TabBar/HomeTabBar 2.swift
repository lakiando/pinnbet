//
//  HomeTabBar.swift
//  PinnBet
//
//  Created by Lazar Andonov on 28/12/2019.
//  Copyright © 2019 Lazar Andonov. All rights reserved.
//

import UIKit

class HomeTabBar: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addControllers()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private func addControllers() {
        tabBar.barTintColor = UIColor.clear
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        guard let bettingViewControoler = storyboard.instantiateViewController(withIdentifier: "BettingConditionsViewController") as? BettingConditionsViewController else { return }
        let bettingItem = UITabBarItem(title: "", image: UIImage(named: "betting"), selectedImage: UIImage(named: "betting_selected"))
        bettingViewControoler.tabBarItem = bettingItem

        guard let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController else { return }
        let homeItem = UITabBarItem(title: "", image: UIImage(named: "pinnNoBG_icon"), selectedImage: UIImage(named: "pinn_icon"))
        homeViewController.tabBarItem = homeItem

        guard let bonusesViewController = storyboard.instantiateViewController(withIdentifier: "BonusesViewController") as? BonusesViewController else { return }
        let bonusesItem = UITabBarItem(title: "", image: UIImage(named: "bonus"), selectedImage: UIImage(named: "bonus_selected"))
        bonusesViewController.tabBarItem = bonusesItem

        let viewControllerList = [
            bettingViewControoler,
            homeViewController,
            bonusesViewController
        ]

        viewControllers = viewControllerList.map {
            UINavigationController(rootViewController: $0)
        }
        
        selectedIndex = 1
    }

}

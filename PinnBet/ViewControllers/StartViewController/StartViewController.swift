//
//  StartViewController.swift
//  PinnBet
//
//  Created by Lazar Andonov on 01/09/2020.
//  Copyright © 2020 Lazar Andonov. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!

    var statusBarStyle: UIStatusBarStyle = .default {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }

    var dataSource: [InitialData] = [
        InitialData(image: UIImage(named: "live-pravila") ?? UIImage(), title: "SPORTSKO", subtitle: "KLADJENJE", id: 0, link: Constans.kladjenjeLink),
        InitialData(image: UIImage(named: "live-pravila") ?? UIImage(), title: "KLADJENJE", subtitle: "UŽIVO", id: 1, link: Constans.uzivoKladjenjeLink),
        InitialData(image: UIImage(named: "live-pravila") ?? UIImage(), title: "VIRTUELNE", subtitle: "IGRE", id: 2, link: Constans.virtIgreLink),
        InitialData(image: UIImage(named: "live-pravila") ?? UIImage(), title: "LUCKY", subtitle: "SIX", id: 3, link: Constans.luckySixLink),
        InitialData(image: UIImage(named: "live-pravila") ?? UIImage(), title: "KAZINO", subtitle: "", id: 4, link: Constans.kazinoLink),
        InitialData(image: UIImage(named: "live-pravila") ?? UIImage(), title: "UŽIVO", subtitle: "KAZINO", id: 5, link: Constans.uzivoKazinoLink)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: false)
    }

}

extension StartViewController {
    func setupUI() {

        statusBarStyle = .lightContent
        collectionView.backgroundColor = .clear
        navigationController?.navigationBar.topItem?.title = ""
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "InitialCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "InitialCollectionViewCell")
        collectionView.collectionViewLayout = GridLayout()

        let button = UIButton()
        button.setImage(UIImage(named: "sideMenu"), for: .normal)
        button.addTarget(self, action: #selector(openSideMenu), for: .touchUpInside)

        let barButtonItem = UIBarButtonItem(customView: button)

        self.navigationItem.leftBarButtonItem = barButtonItem
    }

    @objc func openSideMenu() {
        showLeftViewAnimated(self)
    }
}

extension StartViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedData = dataSource[indexPath.row]
        debugPrint("collection select item: \(selectedData)")
        handleItemSelection(item: selectedData)
    }

    func handleItemSelection(item: InitialData) {
        openHome(data: item)
    }

    func openHome(data: InitialData) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController else { return }
        homeVC.data = data
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
}

extension StartViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InitialCollectionViewCell", for: indexPath) as? InitialCollectionViewCell else { return UICollectionViewCell() }
        let data = dataSource[indexPath.row]
        cell.configureCell(data: data)
        return cell
    }
}

extension StartViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionViewLayout as! GridLayout

        let availableWidth = collectionView.bounds.size.width
        let columns = 2
        var itemTotalWidth = availableWidth - CGFloat(columns - 1) * layout.minimumInteritemSpacing
        itemTotalWidth -= (layout.sectionInset.left + layout.sectionInset.right)

        let itemWidth = itemTotalWidth / CGFloat(columns)
        return CGSize(width: itemWidth, height: itemWidth * 1.3)
    }
}

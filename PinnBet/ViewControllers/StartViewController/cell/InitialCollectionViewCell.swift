//
//  InitialCollectionViewCell.swift
//  PinnBet
//
//  Created by Lazar Andonov on 01/09/2020.
//  Copyright © 2020 Lazar Andonov. All rights reserved.
//

import UIKit

class InitialCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!

    func configureCell(data: InitialData) {
        imageView.image = data.image
        titleLabel.text = data.title

        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        let underlineAttributedString = NSAttributedString(string: data.subtitle, attributes: underlineAttribute)
        subTitleLabel.attributedText = underlineAttributedString
        subTitleLabel.text = data.subtitle
    }

}

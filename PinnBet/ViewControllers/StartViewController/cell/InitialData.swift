//
//  InitialData.swift
//  PinnBet
//
//  Created by Lazar Andonov on 01/09/2020.
//  Copyright © 2020 Lazar Andonov. All rights reserved.
//

import Foundation
import UIKit

struct InitialData {
    var image: UIImage
    var title: String
    var subtitle: String
    var id: Int
    var link: String
}

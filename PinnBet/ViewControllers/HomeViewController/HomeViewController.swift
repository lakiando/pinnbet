//
//  ViewController.swift
//  PinnBet
//
//  Created by Lazar Andonov on 26/12/2019.
//  Copyright © 2019 Lazar Andonov. All rights reserved.
//

import UIKit
import WebKit

class HomeViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    var data: InitialData!

    var systemVersion = UIDevice.current.systemVersion

    var statusBarStyle: UIStatusBarStyle = .default {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupWebView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    private func setupWebView() {
        let titleString = data.title + " " + data.subtitle
        title = titleString.capitalized
        statusBarStyle = .lightContent
        webView.scrollView.delegate = self
        webView.navigationDelegate = self
        indicator.startAnimating()
        let request = URLRequest(url: URL(string: data.link)!)
        webView.load(request)
    }
}

extension HomeViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        indicator.isHidden = true
        indicator.stopAnimating()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        indicator.isHidden = true
        indicator.stopAnimating()
    }
}

extension HomeViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
    }
}


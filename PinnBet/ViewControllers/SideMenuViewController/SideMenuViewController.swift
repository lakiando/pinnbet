//
//  SideMenuViewController.swift
//  PinnBet
//
//  Created by Lazar Andonov on 01/09/2020.
//  Copyright © 2020 Lazar Andonov. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let sideMenuItems = SideMenuItem.allCases

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

}

extension SideMenuViewController {
    private func setup() {
        view.backgroundColor = UIColor.lightGray
        tableView.backgroundColor = .clear
        let nib = UINib(nibName: SideMenuTableViewCell.cellIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: SideMenuTableViewCell.cellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
}

extension SideMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

extension SideMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuTableViewCell.cellIdentifier, for: indexPath) as? SideMenuTableViewCell else { return UITableViewCell() }

        let item = sideMenuItems[indexPath.row]

        cell.titleLabel.text = item.title

        return cell
    }
}

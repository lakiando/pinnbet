//
//  SideMenuTableViewCell.swift
//  PinnBet
//
//  Created by Lazar Andonov on 01/09/2020.
//  Copyright © 2020 Lazar Andonov. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    static let cellIdentifier = "SideMenuTableViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        selectionStyle = .none
        titleLabel.textColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

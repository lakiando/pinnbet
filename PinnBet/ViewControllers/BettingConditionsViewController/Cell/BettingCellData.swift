//
//  BettingCellData.swift
//  PinnBet
//
//  Created by Lazar Andonov on 28/12/2019.
//  Copyright © 2019 Lazar Andonov. All rights reserved.
//

import Foundation
import UIKit

struct BettingCellData {
    var title: String
    var image: UIImage
    var betDescription: String

    init(title: String, image: UIImage, description: String) {
        self.title = title
        self.image = image
        self.betDescription = description
    }
}

//
//  BettingCollectionViewCell.swift
//  PinnBet
//
//  Created by Lazar Andonov on 28/12/2019.
//  Copyright © 2019 Lazar Andonov. All rights reserved.
//

import UIKit

class BettingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    func configureCell(title: String, image: UIImage) {
        imageView.image = image
        titleLabel.text = title
    }
}

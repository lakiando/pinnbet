//
//  BettingConditionsViewController.swift
//  PinnBet
//
//  Created by Lazar Andonov on 28/12/2019.
//  Copyright © 2019 Lazar Andonov. All rights reserved.
//

import UIKit

class BettingConditionsViewController: UIViewController {

    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!

    var dataSource: [BettingCellData] = [
        BettingCellData(title: "FUDBAL", image: UIImage(named: "fudbal") ?? UIImage(), description: Constans.fudbalUslovi),
        BettingCellData(title: "KOŠARKA", image: UIImage(named: "kosarka") ?? UIImage(), description: Constans.kosarkaUslovi),
        BettingCellData(title: "KOŠARKA 3x3", image: UIImage(named: "kosarka-3x3") ?? UIImage(), description: Constans.kosarka3x3Uslovi),
        BettingCellData(title: "TENIS", image: UIImage(named: "tenis") ?? UIImage(), description: Constans.tenisUslovi),
        BettingCellData(title: "HOKEJ", image: UIImage(named: "hokej") ?? UIImage(), description: Constans.hokejUslovi),
        BettingCellData(title: "BEJZBOL", image: UIImage(named: "baseball") ?? UIImage(), description: Constans.bejzbolUslovi),
        BettingCellData(title: "AMERIČKI FUDBAL", image: UIImage(named: "am-fudbal") ?? UIImage(), description: Constans.americkiFudbalUslovi),
        BettingCellData(title: "ODBOJKA", image: UIImage(named: "odbojka") ?? UIImage(), description: Constans.odbojskaUslovi),
        BettingCellData(title: "RUKOMET", image: UIImage(named: "rukomet") ?? UIImage(), description: Constans.rukometUslovi),
        BettingCellData(title: "VATERPOLO", image: UIImage(named: "vaterpolo") ?? UIImage(), description: Constans.vaterpoloUslovi),
        BettingCellData(title: "FUDBAL SPECIJAL", image: UIImage(named: "fudbal-specijal") ?? UIImage(), description: Constans.fudbalSpecijalUslovi),
        BettingCellData(title: "KOŠARKA SPECIJAL", image: UIImage(named: "kosarka-specijal") ?? UIImage(), description: Constans.kosarkaSpecijalUslovi),
        BettingCellData(title: "FUTSAL", image: UIImage(named: "futsal") ?? UIImage(), description: Constans.futsalUslovi),
        BettingCellData(title: "FORMULA 1", image: UIImage(named: "formula") ?? UIImage(), description: Constans.formulaUslovi),
        BettingCellData(title: "SNUKER", image: UIImage(named: "snuker") ?? UIImage(), description: Constans.snookerUslovi),
        BettingCellData(title: "UŽIVO KLAĐENJE – PRAVILA IGRE U SLUČAJU PREKIDA", image: UIImage(named: "live-pravila") ?? UIImage(), description: Constans.uzivoKladjenjeUslovi),
        BettingCellData(title: "RAGBI", image: UIImage(named: "ragbi") ?? UIImage(), description: Constans.ragbiUslovi),
        BettingCellData(title: "GOLOVI U LIGI", image: UIImage(named: "golovi-u-ligi") ?? UIImage(), description: Constans.goloviULigiUslovi),
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(false, animated: false)
    }

}

extension BettingConditionsViewController {
    func setupUI() {
        navigationController?.navigationBar.topItem?.title = "Uslovi Kladjenja"
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "BettingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BettingCollectionViewCell")
    }
}

extension BettingConditionsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedData = dataSource[indexPath.row]
        debugPrint("collection select item: \(selectedData)")
        showBetDescription(data: selectedData)
    }

    func showBetDescription(data: BettingCellData) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let descriptionVC = storyboard.instantiateViewController(withIdentifier: "BetConditionViewController") as? BetConditionViewController else { return }
        self.present(descriptionVC, animated: true, completion: nil)
        descriptionVC.titleLabel.text = data.title
        descriptionVC.textView.text = data.betDescription
    }
}

extension BettingConditionsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BettingCollectionViewCell", for: indexPath) as? BettingCollectionViewCell else { return UICollectionViewCell() }
        let betData = dataSource[indexPath.row]
        cell.configureCell(title: betData.title, image: betData.image)
        return cell
    }
}

extension BettingConditionsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionViewLayout as! GridLayout

        let availableWidth = collectionView.bounds.size.width
        let columns = (availableWidth / 4 > 150) ? 4 : 2
        var itemTotalWidth = availableWidth - CGFloat(columns - 1) * layout.minimumInteritemSpacing
        itemTotalWidth -= (layout.sectionInset.left + layout.sectionInset.right)

        let itemWidth = itemTotalWidth / CGFloat(columns)
        return CGSize(width: itemWidth, height: itemWidth * 0.75)
    }
}

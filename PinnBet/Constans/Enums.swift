//
//  Enums.swift
//  PinnBet
//
//  Created by Lazar Andonov on 01/09/2020.
//  Copyright © 2020 Lazar Andonov. All rights reserved.
//

import Foundation

enum SideMenuItem: CaseIterable {
    case profile
    case casino
    case terms
    case home

    var title: String {
        switch self {
        case .profile:
            return "Profil"
        case .casino:
            return "Kazino"
        case .terms:
            return "Uslovi i pravila korišćenja"
        case .home:
            return "Početna"
        }
    }
}

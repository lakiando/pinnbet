//
//  Constans.swift
//  PinnBet
//
//  Created by Lazar Andonov on 28/12/2019.
//  Copyright © 2019 Lazar Andonov. All rights reserved.
//

import Foundation

class Constans {

    static let kladjenjeLink = "https://www.pinnbet.com/prematch/today"
    static let uzivoKladjenjeLink = "https://www.pinnbet.com/live"
    static let virtIgreLink = "https://www.pinnbet.com/prematch/virtual"
    static let luckySixLink = "https://www.pinnbet.com/prematch/virtual/LuckySix"
    static let kazinoLink = "https://www.pinnbet.com/casino"
    static let uzivoKazinoLink = "https://www.pinnbet.com/casino?GameType=livecasino"

    static let bonusiTitle = """
    Bonus Ostvaruju
    mečevi sa minimalnom
    kvotom 1.15.
    """

    static let bonusiPravila = """
    *pravila bonusa se odnose
    na sve tikete kladjenja
    iz standardne ponude,
    kladjenja uzivo i mix ponude.
    """

    static let fudbalUslovi = """
    KONAČAN ISHOD
    ki1 Domaćin pobeđuje na utakmici
    kiX Na utakmici nema pobednika
    ki2 Gost pobeđuje na utakmici

    DUPLA ŠANSA
    dš1X Domaćin pobeđuje na utakmici ili na utakmici nema pobednika
    dš12 Domaćin pobeđuje na utakmici ili gost pobeđuje na utakmici
    dšX2 Na utakmici nema pobednika ili gost pobeđuje na utakmici

    I POLUVREME
    pp1 Domaćin pobeđuje u prvom poluvremenu
    ppX Na kraju prvog poluvremena biće nerešeno
    pp2 Gost pobeđuje u prvom poluvremenu

    II POLUVREME
    dp1 Domaćin pobeđuje u drugom poluvremenu
    dpX Na kraju drugog poluvremena biće nerešeno
    dp2 Gost pobeđuje u drugom poluvremenu
    
    POLUVREME – KRAJ
    1-1 Domaćin će voditi na poluvremenu i pobediti na kraju utakmice
    1-X Domaćin će voditi na poluvremenu, ali će na kraju utakmice biti nerešeno
    1-2 Domaćin će voditi na poluvremenu, ali će na kraju utakmice pobediti gost
    X-1 Na poluvremenu će biti nerešeno, ali će na kraju pobediti domaćin
    X-X I na poluvremenu i na kraju utakmice biće nerešeno
    X-2 Na poluvremenu će biti nerešeno, ali će na kraju pobediti gost
    2-1 Gost će voditi na poluvremenu, ali će na kraju utakmice pobediti domaćin
    2-X Gost će voditi na poluvremenu, ali će na kraju utakmice biti nerešeno
    2-2 Gost će voditi na poluvremenu i pobediti na kraju utakmice
    ne 1-1 Domaćin neće voditi na poluvremenu niti će pobediti na kraju utakmice
    ne 2-2 Gost neće voditi na poluvremenu niti će pobediti na kraju utakmice

    HENDIKEP (1,5)
    hk15 1 Domaćin će pobediti sa 2 ili više golova razlike
    hk15 2 Gost će pobediti, biće nerešeno ili će izgubiti sa 1 golom razlike

    HENDIKEP (2,5)
    hk25 1 Domaćin će pobediti sa 3 ili više golova razlike
    hk25 2 Gost će pobediti, biće nerešeno ili će izgubiti sa najviše 2 gola razlike

    AZIJSKI HENDIKEP (1)
    ah1 1 Ako domaćin pobedi sa 2 ili više golova razlike, tip je dobitan, a ako pobedi sa 1 golom razlike, igraču se vraća ceo ulog; ako se meč završi nerešeno ili domaćin izgubi, tip je gubitan
    ah1 2 Ako gost pobedi ili se meč završi nerešenim rezultatom, tip je dobitan, a ako izgubi sa 1 golom razlike, igraču se vraća ceo ulog; ako gost izgubi sa 2 ili više golova razlike, tip je gubitan

    AZIJSKI HENDIKEP (2)
    ah2 1 Ako domaćin pobedi sa 3 ili više golova razlike, tip je dobitan, a ako pobedi sa tačno dva gola razlike, igraču se vraća ceo ulog; ako se meč završi pobedom domaćina sa 1 golom razlike, nerešenim rezultatom ili domaćin izgubi, tip je gubitan
    ah2 2 Ako gost izgubi sa 1 golom razlike, bude nerešeno ili pobedi, tip je dobitan; ako gost izgubi sa 2 gola razlike, igraču se vraća ceo ulog; ako gost izgubi sa 3 ili više golova razlike, tip je gubitan

    AZIJSKI HENDIKEP GOLOVI (2)
    ahg2< Na meču će biti ukupno manje od 2 gola, ukoliko bude tačno 2, igraču se vraća ceo ulog; tip je gubitan ukoliko bude više od 2 gola
    ahg2> Na meču će biti ukupno više od 2 gola, ukoliko bude tačno 2, igraču se vraća ceo ulog; tip je gubitan ukoliko bude manje od 2 gola

    AZIJSKI HENDIKEP GOLOVI (3)
    ahg3< Na meču će biti ukupno manje od 3 gola, ukoliko bude tačno 3, igraču se vraća ceo ulog; tip je gubitan ukoliko bude više od 3 gola
    ahg3> Na meču će biti ukupno više od 3 gola, ukoliko bude tačno 3, igraču se vraća ceo ulog; tip je gubitan ukoliko bude manje od 3 gola

    UKUPNO GOLOVA
    ug 0-1 Na utakmici neće biti golova ili će biti tačno 1 gol
    ug 0-2 Na utakmici neće biti golova ili će biti 1 ili 2
    ug 0-3 Na utakmici neće biti golova ili će biti 1, 2 ili 3
    ug 2+ Na utakmici će biti 2 ili više golova
    ug 1-2 Na utakmici će biti 1 ili 2 gola
    ug 1-3 Na utakmici će biti 1, 2 ili 3 gola
    ug 2-3 Na utakmici će biti 2 ili 3 gola
    ug 2-4 Na utakmici će biti 2, 3 ili 4 gola
    ug 2-5 Na utakmici će biti 2, 3, 4 ili 5 golova
    ug 3+ Na utakmici će biti 3 ili više golova
    ug 3-4 Na utakmici će biti 3 ili 4 gola
    ug 3-5 Na utakmici će biti 3, 4 ili 5 golova
    ug 4+ Na utakmici će biti 4 ili više golova
    ug 4-5 Na utakmici će biti 4 ili 5 golova
    ug 4-6 Na utakmici će biti 4, 5 ili 6 golova
    ug 5+ Na utakmici će biti 5 ili više golova
    ug 6+ Na utakmici će biti 6 ili više golova
    ug 7+ Na utakmici će biti 7 ili više golova

    GOLOVI PRVO POLUVREME
    1p 0 U prvom poluvremenu neće biti golova
    1p 0-1 U prvom poluvremenu neće biti golova ili će biti tačno 1 gol
    1p 1+ U prvom poluvremenu će biti 1 ili više golova
    1p 1-2 U prvom poluvremenu biće 1 ili 2 gola
    1p 2+ U prvom poluvremenu biće 2 ili više golova
    1p 2-3 U prvom poluvremenu biće 2 ili 3 gola
    1p 3+ U prvom poluvremenu biće 3 ili više golova

    GOLOVI DRUGO POLUVREME
    2p 0 U drugom poluvremenu neće biti golova
    2p 0-1 U drugom poluvremenu neće biti golova ili će biti tačno 1 gol
    2p 1+ U drugom poluvremenu će biti 1 ili više golova
    2p 1-2 U drugom poluvremenu biće 1 ili 2 gola
    2p 2+ U drugom poluvremenu biće 2 ili više golova
    2p 2-3 U drugom poluvremenu biće 2 ili 3 gola
    2p 3+ U drugom poluvremenu biće 3 ili više golova

    TIM 1 GOLOVA PRVO POLUVREME
    1tm1 0 Domaćin neće dati gol u prvom poluvremenu
    1tm1 0-1 Domaćin neće dati gol u prvom poluvremenu ili će dati 1 gol
    1tm1 1+ Domaćin će dati 1 ili više golova u prvom poluvremenu
    1tm1 1-2 Domaćin će dati 1 ili 2 gola u prvom poluvremenu
    1tm1 2+ Domaćin će dati 2 ili više golova u prvom poluvremenu
    1tm1 3+ Domaćin će dati 3 ili više golova u prvom poluvremenu
    
    TIM 2 GOLOVA PRVO POLUVREME
    1tm2 0 Gost neće dati gol u prvom poluvremenu
    1tm2 0-1 Gost neće dati gol u prvom poluvremenu ili će dati 1 gol
    1tm2 1+ Gost će dati 1 ili više golova u prvom poluvremenu
    1tm2 1-2 Gost će dati 1 ili 2 gola u prvom poluvremenu
    1tm2 2+ Gost će dati 2 ili više golova u prvom poluvremenu
    1tm2 3+ Gost će dati 3 ili više golova u prvom poluvremenu

    TIM 1 GOLOVA DRUGO POLUVREME
    2tm1 0 Domaćin neće dati gol u drugom poluvremenu
    2tm1 0-1 Domaćin neće dati gol u drugom poluvremenu ili će dati 1 gol
    2tm1 1+ Domaćin će dati 1 ili više golova u drugom poluvremenu
    2tm1 1-2 Domaćin će dati 1 ili 2 gola u drugom poluvremenu
    2tm1 2+ Domaćin će dati 2 ili više golova u drugom poluvremenu
    2tm1 3+ Domaćin će dati 3 ili više golova u drugom poluvremenu

    TIM 2 GOLOVA DRUGO POLUVREME
    2tm2 0 Gost neće dati gol u drugom poluvremenu
    2tm2 0-1 Gost neće dati gol u drugom poluvremenu ili će dati 1 gol
    2tm2 1+ Gost će dati 1 ili više golova u drugom poluvremenu
    2tm2 1-2 Gost će dati 1 ili 2 gola u drugom poluvremenu
    2tm2 2+ Gost će dati 2 ili više golova u drugom poluvremenu
    2tm2 3+ Gost će dati 3 ili više golova u drugom poluvremenu
    
    TIM 1 UKUPNO GOLOVA
    tm1 0 Domaćin neće dati gol na utakmici
    tm1 0-1 Domaćin neće dati gol na utakmici ili će dati 1 gol
    tm1 1+ Domaćin će dati 1 ili više golova na utakmici
    tm1 1-2 Domaćin će dati 1 ili 2 gola na utakmici
    tm1 2+ Domaćin će dati 2 ili više golova na utakmici
    tm1 2-3 Domaćin će dati 2 ili 3 gola na utakmici
    tm1 3+ Domaćin će dati 3 ili više golova na utakmici

    TIM 2 UKUPNO GOLOVA
    tm2 0 Gost neće dati gol na utakmici
    tm2 0-1 Gost neće dati gol na utakmici ili će dati 1 gol
    tm2 1+ Gost će dati 1 ili više golova na utakmici
    tm2 1-2 Gost će dati 1 ili 2 gola na utakmici
    tm2 2+ Gost će dati 2 ili više golova na utakmici
    tm2 2-3 Gost će dati 2 ili 3 gola na utakmici
    tm2 3+ Gost će dati 3 ili više golova na utakmici

    KOMBINACIJE
    1&2-3 Pobediće domaćin i ukupan broj golova na utakmici biće od 2 do 3
    1&3+ Pobediće domaćin i na utakmici će biti postignuto 3 ili više golova
    1&4+ Pobediće domaćin i na utakmici će biti postignuto 4 ili više golova
    1&2+I Pobediće domaćin i u prvom poluvremenu će biti postignuto 2 ili više golova
    1&2-3I Pobediće domaćin i u prvom poluvremenu će biti postignuta 2 ili 3 gola
    1&t1 2+ Pobediće domaćin i pritom postići 2 ili više golova
    1-1&2+ Domaćin će voditi na poluvremenu i pobediti na kraju i ukupno će na utakmici biti postignuto 2 ili više golova
    1-1&2-3 Domaćin će voditi na poluvremenu i pobediti na kraju i ukupno će na utakmici biti postignuto 2 ili 3 gola
    1-1&3+ Domaćin će voditi na poluvremenu i pobediti na kraju i ukupno će na utakmici biti postignuto 3 ili više golova
    1-1&4+ Domaćin će voditi na poluvremenu i pobediti na kraju i ukupno će na utakmici biti postignuto 4 ili više golova
    1-1&2+I Domaćin će voditi na poluvremenu i pobediti na kraju i u prvom poluvremenu će biti postignuto 2 ili više golova
    1-1&2-3I Domaćin će voditi na poluvremenu i pobediti na kraju i u prvom poluvremenu će biti postignuto 2 ili 3 gola
    1-1&hk1 Domaćin će voditi na poluvremenu bilo kojim rezultatom i pobediti na kraju utakmice sa 2 ili više golova razlike
    2&2-3 Pobediće gost i ukupan broj golova na utakmici biće od 2 do 3
    2&3+ Pobediće gost i na utakmici će biti postignuto 3 ili više golova
    2&4+ Pobediće gost i na utakmici će biti postignuto 4 ili više golova
    2&2+I Pobediće gost i u prvom poluvremenu će biti postignuto 2 ili više golova
    2&2-3I Pobediće gost i u prvom poluvremenu će biti postignuta 2 ili 3 gola
    2&t2 2+ Pobediće gost i pritom će postići 2 ili više golova
    2-2&2+ Gost će voditi na poluvremenu i pobediti na kraju i ukupno će na utakmici biti postignuto 2 ili više golova
    2-2&2-3 Gost će voditi na poluvremenu i pobediti na kraju i ukupno će na utakmici biti postignuto 2 ili 3 gola
    2-2&3+ Gost će voditi na poluvremenu i pobediti na kraju i ukupno će na utakmici biti postignuto 3 ili više golova
    2-2&4+ Gost će voditi na poluvremenu i pobediti na kraju i ukupno će na utakmici biti postignuto 4 ili više golova
    2-2&2+I Gost će voditi na poluvremenu i pobediti na kraju i u prvom poluvremenu će biti postignuto 2 ili više golova
    2-2&2-3I Gost će voditi na poluvremenu i pobediti na kraju i u prvom poluvremenu će biti postignuto 2 ili 3 gola
    2-2&hk2 Gost će voditi na poluvremenu bilo kojim rezultatom i pobediti na kraju utakmice sa 2 ili više golova razlike
    dp11&2+I Domaćin će dobiti i prvo i drugo poluvreme i u prvom poluvremenu će biti postignuto 2 ili više golova
    dp22&2+I Gost će dobiti i prvo i drugo poluvreme i u prvom poluvremenu će biti postignuto 2 ili više golova
    dp11&2-3I Domaćin će dobiti i prvo i drugo poluvreme i u prvom poluvremenu će biti postignuto 2 ili 3 gola
    dp22&2-3I Gost će dobiti i prvo i drugo poluvreme i u prvom poluvremenu će biti postignuto 2 ili 3 gola
    dp11&4+ Domaćin će dobiti i prvo i drugo poluvreme I ukupno će na utakmici biti postignuto 4 ili više golova
    dp22&4+ Gost će dobiti i prvo i drugo poluvreme I ukupno će na utakmici biti postignuto 4 ili više golova

    UKUPNO GOLOVA KOMBINACIJE
    1+I&1+II I u prvom i u drugom poluvremenu biće postignuto 1 ili više golova
    1+I&2+II U prvom poluvremenu biće postignuto 1 ili više golova, a u drugom poluvremenu biće postignuto 2 ili više golova
    2+I&1+II U prvom poluvremenu biće postignuto 2 ili više golova, a u drugom poluvremenu biće postignuto 1 ili više golova
    2+I&2+II I u prvom i u drugom poluvremenu biće postignuto 2 ili više golova
    2+I&4+ U prvom poluvremenu biće postignuto 2 ili više golova i ukupno na utakmici biće postignuto 4 ili više golova
    2-3I&4+ U prvom poluvremenu biće postignuto 2 ili 3 gola i ukupno na utakmici biće postignuto 4 ili više golova

    TIM1 GOLOVI KOMBINACIJE
    tm1 1+I&2+II Domaćin će dati 1 ili više golova u prvom i 2 ili više golova u drugom poluvremenu
    tm1 2+I&1+II Domaćin će dati 2 ili više golova u prvom i 1 ili više golova u drugom poluvremenu
    tm1 2+I&2+II Domaćin će dati 2 ili više golova u prvom i 2 ili više golova u drugom poluvremenu
    tm1 1&2-3 Domaćin će pobediti i pritom će postići 2 ili 3 gola na utakmici
    tm1 2+&gg Domaćin će postići 2 ili više golova,a gost će postići 1 ili više golova na utakmici
    tm1 12gg Domaćin će postići po 1 ili više golova u oba poluvremena
    tm1 12ng Domaćin neće dati gol bar u jednom poluvremenu

    TIM2 GOLOVI KOMBINACIJE
    tm2 1+I&2+II Gost će dati 1 ili više golova u prvom i 2 ili više golova u drugom poluvremenu
    tm2 2+I&1+II Gost će dati 2 ili više golova u prvom i 1 ili više golova u drugom poluvremenu
    tm2 2+I&2+II Gost će dati 2 ili više golova u prvom i 2 ili više golova u drugom poluvremenu
    tm2 1&2-3 Gost će pobediti i pritom će postići 2 ili 3 gola na utakmici
    tm2 2+&gg Gost će postići 2 ili više golova,a domaćin će postići 1 ili više golova na utakmici
    tm2 12gg Gost će postići po 1 ili više golova u oba poluvremena
    tm2 12ng Gost neće dati gol bar u jednom poluvremenu

    OBA TIMA DAJU GOL
    gg Oba tima će dati po 1 ili više golova na utakmici
    gg2+ Oba tima će dati po 2 ili više golova na utakmici
    ng Bar jedan tim neće dati gol na utakmici
    1gg Oba tima će dati po 1 ili više golova u prvom poluvremenu
    2gg Oba tima će dati po 1 ili više golova u drugom poluvremenu
    1gg&2gg Oba tima će dati po 1 ili više golova i u prvom i u drugom poluvremenu
    1&gg Domaćin će pobediti I oba tima će dati po 1 ili više golova na utakmici
    2&gg Gost će pobediti I oba tima će dati po 1 ili više golova na utakmici
    1-1&gg Domaćin će voditi na poluvremenu i pobediti na kraju i oba tima će dati po 1 ili više
    golova na utakmici
    2-2&gg Gost će voditi na poluvremenu i pobediti na kraju i oba tima će dati po 1 ili više golova na utakmici
    gg&3+ Oba tima će dati po 1 ili više golova i ukupni broj golova na utakmici biće 3 ili više
    gg&4+ Oba tima će dati po 1 ili više golova i ukupni broj golova na utakmici biće 4 ili više
    gg&2+I Oba tima će dati po 1 ili više golova na utakmici i u prvom poluvremenu će biti postignuto 2 ili više golova
    gg&2-3I Oba tima će dati po 1 ili više golova na utakmici i u prvom poluvremenu će biti postignuto 2 ili 3 gola
    
    TAČAN REZULTAT
    r0:0 Tačan rezultat utakmice će biti 0:0
    r0:1 Tačan rezultat utakmice će biti 0:1
    r0:2 Tačan rezultat utakmice će biti 0:2
    r0:3 Tačan rezultat utakmice će biti 0:3
    r1:0 Tačan rezultat utakmice će biti 1:0
    r1:1 Tačan rezultat utakmice će biti 1:1
    r1:2 Tačan rezultat utakmice će biti 1:2
    r1:3 Tačan rezultat utakmice će biti 1:3
    r2:0 Tačan rezultat utakmice će biti 2:0
    r2:1 Tačan rezultat utakmice će biti 2:1
    r2:2 Tačan rezultat utakmice će biti 2:2
    r2:3 Tačan rezultat utakmice će biti 2:3
    r3:0 Tačan rezultat utakmice će biti 3:0
    r3:1 Tačan rezultat utakmice će biti 3:1
    r3:2 Tačan rezultat utakmice će biti 3:2
    r3:3 Tačan rezultat utakmice će biti 3:3
    OSTALI REZULTATI Tačan rezultat utakmice će biti neki od ostalih neponuđenih rezultata

    PRVI DAJE GOL
    pdg1 Domaćin postiže prvi gol na utakmici
    pdg2 Gost postiže prvi gol na utakmici

    ŠANSA
    1 v pp1 Domaćin će voditi na poluvremenu ili pobediti na kraju utakmice ili i jedno i drugo
    X v ppX Biće nerešen rezultat ili na poluvremenu ili na kraju utakmice ili i jedno i drugo
    2 v pp2 Gost će voditi na poluvremenu ili pobediti na kraju utakmice ili i jedno i drugo
    1 v 3+ Domaćin će pobediti na kraju ili će biti postignuto 3 i više golova ili i jedno i drugo
    2 v 3+ Gost će pobediti na kraju ili će biti postignuto 3 i više golova ili i jedno i drugo
    1 v 4+ Domaćin će pobediti na kraju ili će biti postignuto 4 i više golova ili i jedno i drugo
    2 v 4+ Gost će pobediti na kraju ili će biti postignuto 4 i više golova ili i jedno i drugo
    1 v 2+I Domaćin će pobediti na kraju utakmice ili će u prvom poluvremenu biti postignuto 2 ili više golova ili i jedno i drugo
    2 v 2+I Gost će pobediti na kraju utakmice ili će u prvom poluvremenu biti postignuto 2 ili više golova ili i jedno i drugo

    VIŠE GOLOVA
    I>II Više golova će biti postignuto u prvom poluvremenu
    I=II U oba poluvremena će biti postignut isti broj golova ili ih neće biti
    I< II Više golova će biti postignuto u drugom poluvremenu

    SIGURNA POBEDA
    sp1 Domaćin će pobediti, a gost neće postići nijedan gol
    sp2 Gost će pobediti, a domaćin neće postići nijedan gol

    SIGURNA POBEDA / HENDIKEP (1.5)
    sphk1 Domaćin će pobediti sa 2 ili više golova razlike, a gost neće postići nijedan gol
    sphk2 Gost će pobediti sa 2 ili više golova razlike, a domaćin neće postići nijedan gol

    DUPLA POBEDA
    dp11 Domaćin će dobiti i prvo i drugo poluvreme
    dp22 Gost će dobiti i prvo i drugo poluvreme
    
    SIGURNA DUPLA POBEDA
    s11 Domaćin će dobiti i prvo i drugo poluvreme, a gost neće postići nijedan gol
    s22 Gost će dobiti i prvo i drugo poluvreme, a domaćin neće postići nijedan gol

    WINNER
    w1 Pobednik utakmice će biti domaćin, a u slučaju nerešenog ishoda kvota je 1
    w2 Pobednik utakmice će biti gost, a u slučaju nerešenog ishoda kvota je 1

    Važeći je regularni tok meča, bez produžetaka (90 minuta plus sudijska nadoknada).
    Za mečeve koji se igraju suprotno pravilima od propisanih poluvremena u trajanju od po 45 min (npr. 2 poluvremena po 35 min, jedno poluvreme od 40 min ili 3 pol po 30 min, itd.) biće proglašeno nevažece klađenje (računaće se kvota 1).

    U slučaju prekida fudbalskog događaja, bilo kada u toku meča, kada utakmica ne bude nastavljena do kraja narednog dana po lokalnom vremenu, sve opklade čiji se ishod realizovao do trenutka prekida proglašavaju se važećim. Sve opklade čiji ishod nije realizovan i mogao je makar teoretski da se promeni, računaće se kvotom jedan.

    Primer: Na utakmici Barselona – Real Madrid prvo poluvreme završeno je rezultatom 2:0. Prekid je usledio u 68. minutu pri rezultatu 3:0. Opklade čiji je ishod završen (na primer – prvo poluvreme, 3+, 0-2, tim daje dva gola...) proglašavaju se važećim. Opklade čiji ishod nije realizovan (na primer – konačan ishod, 4+, 45/90...) računaće se kvotom jedan.

    U slučaju odlaganja fudbalske utakmice, meč će se smatrati validnim ukoliko bude odigran do kraja narednog dana po lokalnom vremenu. Nakon isteka ovog roka, za izabrani meč računaće se kvota jedan.
    """

    static let kosarkaUslovi = """
    KONAČAN ISHOD BEZ PRODUŽETAKA
    1 U regularnom toku meča će pobediti domaćin
    X U regularnom toku meč će se završiti nerešenim ishodom
    2 U regularnom toku meča će pobediti gost

    KONAČNI ISHOD MEČA (UKLJUČUJUĆI PRODUŽETKE)
    K1 U meču će pobediti domaćin
    K2 U meču će pobediti gost

    ISHOD PRVOG POLUVREMENA MEČA
    P1 Domaćin pobeđuje u prvom poluvremenu
    PX Na kraju prvog poluvremena biće nerešeno
    P2 Gost pobeđuje u prvom poluvremenu

    ISHOD PRVE ČETVRTINE MEČA
    1.cet(1) Domaćin pobeđuje u prvoj četvrtini
    1.cet(X) Na kraju prve četvrtine biće nerešeno
    1.cet(2) Gost pobeđuje u prvoj četvrtini

    KONAČNI ISHOD SA HENDIKEPOM (UKLJUČUJUĆI PRODUŽETKE)
    H1 U meču će pobediti domaćin uz uračunat hendikep
    H2 U meču će pobediti gost uz uračunat hendikep

    KONAČNI ISHOD PRVOG POLUVREMENA SA HENDIKEPOM
    HP1 U poluvremenu će pobediti domaćin uz uračunat hendikep
    HP2 U poluvremenu će pobediti gost uz uračunat hendikep

    KONAČNI ISHOD PRVE ČETVRTINE SA HENDIKEPOM
    1/4h1 U prvoj četvrtini će pobediti domaćin uz uračunat hendikep
    1/4h2 U prvoj četvrtini će pobediti gost uz uračunat hendikep

    UKUPNO POENA U MEČU (UKLJUČUJUĆI PRODUŽETKE)
    bpk< Ukupan broj poena na meču će biti manji od zadate granice
    bpk> Ukupan broj poena na meču će biti veći od zadate granice

    UKUPAN BROJ POENA U I POLUVREMENU
    bpp< Ukupan broj poena na kraju I poluvremena će biti manji od zadate granice
    bpp> Ukupan broj poena na kraju I poluvremena će biti veći od zadate granice

    UKUPAN BROJ POENA U I ČETVRTINI
    bp1/4< Ukupan broj poena na kraju I četvrtine će biti manji od zadate granice
    bp1/4> Ukupan broj poena na kraju I četvrtine će biti veći od zadate granice

    ALTERNATIVNI HENDIKEP (UKLJUČUJUĆI PRODUŽETKE)
    HK1 (-3, -2, -1), (+3, +2, +1) U meču će pobediti domaćin uz uračunat alternativni hendikep
    HK2 (-3, -2, -1), (+3, +2, +1) U meču će pobediti gost uz uračunat alternativni hendikep

    DOMAĆIN UKUPNO POENA NA MEČU (UKLJUČUJUĆI PRODUŽETKE)
    ddp< Domaćin će postiči manje poena u odnosu na zadatu granicu
    ddp> Domaćin će postiči više poena u odnosu na zadatu granicu

    DOMAĆIN UKUPNO POENA U I POLUVREMENU
    ddpp< Domaćin će postiči manje poena u odnosu na zadatu granicu
    ddpp> Domaćin će postiči više poena u odnosu na zadatu granicu

    DOMAĆIN UKUPNO POENA U I ČETVRTINI
    ddp1/4< Domaćin će postiči manje poena u odnosu na zadatu granicu
    ddp1/4> Domaćin će postiči više poena u odnosu na zadatu granicu

    GOST UKUPNO POENA NA MEČU (UKLJUČUJUĆI PRODUŽETKE)
    gdp< Gost će postiči manje poena u odnosu na zadatu granicu
    gdp> Gost će postiči više poena u odnosu na zadatu granicu

    GOST UKUPNO POENA U I POLUVREMENU
    gdpp< Gost će postiči manje poena u odnosu na zadatu granicu
    gdpp> Gost će postiči više poena u odnosu na zadatu granicu

    GOST UKUPNO POENA U I ČETVRTINI
    gdp1/4< Gost će postiči manje poena u odnosu na zadatu granicu
    gdp1/4> Gost će postiči više poena u odnosu na zadatu granicu

    DUPLA POBEDA (UKLJUČUJUĆI PRODUŽETKE)
    dp1 Domaćin će ostvariti pobedu u oba poluvremena
    dp2 Gost će ostvariti pobedu u oba poluvremena

    ALTERNATIVNI POENI (UKLJUČUJUĆI PRODUŽETKE)
    bpk<(-3, -2, -1), (+3, +2, +1) Ukupan broj poena na meču će biti manji od alternativne granice
    bpk>(-3, -2, -1), (+3, +2, +1) Ukupan broj poena na meču će biti veći od alternativne granice

    KOMBINACIJE (UKLJUČUJUĆI PRODUŽETKE)
    1&- Domaćin će pobediti i pašće manje poena u odnosu na zadatu granicu
    1&+ Domaćin će pobediti i pašće više poena u odnosu na zadatu granicu
    2&- Gost će pobediti i pašće manje poena u odnosu na zadatu granicu
    2&+ Gost će pobediti i pašće više poena u odnosu na zadatu granicu
    h1&- Domaćin će pobediti uz uračunat hendikep i pašće manje poena u odnosu na zadatu granicu
    h1&+ Domaćin će pobediti uz uračunat hendikep i pašće više poena u odnosu na zadatu granicu
    h2&- Gost će pobediti uz uračunat hendikep i pašće manje poena u odnosu na zadatu granicu
    h2&+ Gost će pobediti uz uračunat hendikep i pašće više poena u odnosu na zadatu granicu

    U slučaju prekida košarkaškog događaja, bilo kada u toku meča, kada utakmica ne bude nastavljena do kraja narednog dana po lokalnom vremenu, sve opklade čiji se ishod realizovao do trenutka prekida proglašavaju se važećim. Sve opklade čiji ishod nije realizovan i mogao je makar teoretski da se promeni, računaće se kvotom jedan.

    U slučaju odlaganja košarkaške utakmice, meč će se smatrati validnim ukoliko bude odigran do kraja narednog dana po lokalnom vremenu. Nakon isteka ovog roka, za izabrani meč računaće se kvota jedan.

    Kod igara KONAČNI ISHOD SA HENDIKEPOM i ALTERNATIVNI HENDIKEP, gde je hendikep na okrugao broj, ukoliko igrač „pogodi“ tačan hendikep, na tom meču računaće se kvota 1. Primer: Na utakmici Real Madrid – Barselona hendikep je -5. Real je pobedio rezultatom 85:80. Ukoliko je igrač odigrao 1 sa hendikepom, na tom meču računaće se kvota 1.

    Kod igara UKUPNO POENA U MEČU, UKUPAN BROJ POENA U I POLUVREMENU, UKUPAN BROJ POENA U I ČETVRTINI, DOMAĆIN UKUPNO POENA NA MEČU, DOMAĆIN UKUPNO POENA U I POLUVREMENU, DOMAĆIN UKUPNO POENA U I ČETVRTINI, GOST UKUPNO POENA NA MEČU, GOST UKUPNO POENA U I POLUVREMENU, GOST UKUPNO POENA U I ČETVRTINI, DOMAĆIN UKUPNO POENA NA MEČU, GOST UKUPNO POENA NA MEČU i ALTERNATIVNI POENI, gde je granica na okrugao broj, ukoliko igrač „pogodi“ tačnu granicu, na tom meču računaće se kvota 1. Primer: Na utakmici Real Madrid – Barselona granica koševa je 165, Real je pobedio rezultatom 85:80 i tačan broj postignutih koševa je 165. Ukoliko je igrač odigrao više ili manje poena na utakmici, na tom meču računaće se kvota 1.

    Kod igre KOMBINACIJE, gde je hendikep ili granica na okrugao broj, igrač pogodi jedan znak u kombinaciji, a „pogodi“ i tačan hendikep/granicu, na tom meču računaće se kvota 1. Primer: Na utakmici Real Madrid – Barselona granica koševa je 165, Real je pobedio rezultatom 85:80 i tačan broj postignutih koševa je 165. Ukoliko je igrač odigrao kombinaciju „kec“ i više poena na utakmici, na tom meču računaće se kvota 1.

    Za igru POLUVREME-KRAJ ne računaju se produžeci.

    Kod igre POLUVREME-KRAJ u slučaju prekida u prvom poluvremenu računaće se kvota 1.00. U slučaju prekida na poluvremenu ili bilo kad u drugom, za sve koji su pogodili ishod prvog poluvremena kvota je 1.00. Svi tipovi na kojima nije pogođen rezultat prvog poluvremena, a došlo je kasnije do prekida, smatraju se nedobitnim.

    Napomena:
    U slučaju nerešenog ishoda važe eventualni produžeci za sve vrste igara. Izuzev pojedinih takmičenja gde postoje revanš mečevi, ukoliko se meč završi nerešeno i proglasi se kao službeni rezultat za igru konačan ishod važi kvota jedan.
    """

    static let kosarka3x3Uslovi = """
    KONAČNI ISHOD MEČA (UKLJUČUJUĆI PRODUŽETKE)
    K1 U meču će pobediti domaćin
    K2 U meču će pobediti gost

    KONAČNI ISHOD SA HENDIKEPOM (UKLJUČUJUĆI PRODUŽETKE)
    H1 U meču će pobediti domaćin uz uračunat hendikep
    H2 U meču će pobediti gost uz uračunat hendikep

    UKUPNO POENA U MEČU (UKLJUČUJUĆI PRODUŽETKE)
    bpk< Ukupan broj poena na meču će biti manji od zadate granice
    bpk> Ukupan broj poena na meču će biti veći od zadate granice

    Napomena: U slučaju nerešenog ishoda važe eventualni produžeci za sve vrste igara. Ukoliko se meč završi nerešeno i proglasi se kao službeni rezultat za igru konačan ishod važi kvota jedan.
    U slučaju prekida košarkaškog događaja, bilo kada u toku meča, kada utakmica ne bude nastavljena do kraja narednog dana po lokalnom vremenu, sve opklade čiji se ishod realizovao do trenutka prekida proglašavaju se važećim. Sve opklade čiji ishod nije realizovan i mogao je makar teoretski da se promeni, računaće se kvotom jedan.
    U slučaju odlaganja košarkaške utakmice, meč će se smatrati validnim ukoliko bude odigran do kraja narednog dana po lokalnom vremenu. Nakon isteka ovog roka, za izabrani meč računaće se kvota jedan.
    """

    static let tenisUslovi = """
    KONAČNI ISHOD MEČA
    K1 U meču će pobediti domaćin
    K2 U meču će pobediti gost

    PRVI SET
    P1 U prvom setu će pobediti domaćin
    P2 U prvom setu će pobediti gost

    HENDIKEP SETOVI
    H1 U meču će pobediti domaćin uz uračunat hendikep
    H2 U meču će pobediti gost uz uračunat hendikep

    HENDIKEP GEMOVI
    HP1 Više gemova u meču osvojiće domaćin uz uračunat hendikep
    HP2 Više gemova u meču osvojiće gost uz uračunat hendikep

    GEMOVI PRVI SET
    < Ukupan broj gemova u prvom setu biće manji od zadate granice
    > Ukupan broj gemova u prvom setu biće veći od zadate granice

    UKUPNO GEMOVA
    < Ukupan broj gemova u meču biće manji od zadate granice
    > Ukupan broj gemova u meču biće veći od zadate granice
    
    U slučaju da se jedan igrač povuče ili bude diskvalifikovan pre početka meča ili tokom prvog seta, sve opklade će se smatrati nevažećim.

    U slučaju da se jedan igrač povuče ili bude diskvalifikovan nakon završetka prvog seta, protivnik će se smatrati za pobednika meča, dok će sve opklade čiji je ishod realizovan do trenutka prekida biti važeće.

    Primer: Na meču Đoković – Nadal, pri rezulatatu 6:3, 4:2, Đoković je predao meč. Opklade koje se odnose na prvi set (na primer – pobednik prvog seta, ukupno gemova u prvom setu...) proglašavaju se važećim. Za igru konačan ishod, kao pobednik se računa Nadal. Opklade čiji ishod nije realizovan (na primer – konačan ishod u hendikepu, ukupan broj gemova...) računaće se kvotom jedan.

    U slučaju prekida ili odlaganja teniskog meča, meč će se smatrati validnim ukoliko bude odigran u roku od 72 časa od najavljenog termina. Nakon isteka ovog vremena, za izabrani meč računaće se kvota jedan.

    Kod igre KOMBINACIJE dva tipa, u slučaju prekida meča u bilo kom momentu računaće se kvota 1.00.
    """

    static let hokejUslovi = """
    KONAČNI ISHOD MEČA
    1 U meču će pobediti domaćin
    X U meču će biti nerešeno
    2 U meču će pobediti gost

    DUPLA ŠANSA
    1X Domacin neće izgubiti meču
    X2 Gost neće izgubiti u meču

    PRVA TREĆINA
    P1 U prvoj trećini će pobediti domaćin
    PX U prvoj trećini će biti nerešeno
    P2 U prvoj trećini će pobediti gost

    UKUPNO GOLOVA - PRVA TREĆINA
    - Ukupan broj golova u prvoj trećini biće manji od zadate granice
    + Ukupan broj golova u prvoj trećini biće veći od zadate granice

    UKUPNO GOLOVA NA MEČU
    - Ukupan broj golova u meču biće manji od zadate granice
    + Ukupan broj golova u meču biće veći od zadate granice

    KOMBINACIJE
    1&< Domaćin će pobediti i pašće manje golova u odnosu na zadatu granicu
    1&> Domaćin će pobediti i pašće više golova u odnosu na zadatu granicu
    2&< Gost će pobediti i pašće manje golova u odnosu na zadatu granicu
    2&> Gost će pobediti i pašće više golova u odnosu na zadatu granicu

    Važeći je regularni tok meča, bez produžetaka.

    U slučaju prekida hokejaškog događaja, bilo kada u toku meča, kada utakmica ne bude nastavljena do kraja narednog dana po lokalnom vremenu, sve opklade čiji se ishod realizovao do trenutka prekida proglašavaju se važećim. Sve opklade čiji ishod nije realizovan i mogao je makar teoretski da se promeni, računaće se kvotom jedan.

    U slučaju odlaganja hokejaške utakmice, meč će se smatrati validnim ukoliko bude odigran do kraja narednog dana po lokalnom vremenu. Nakon isteka ovog roka, za izabrani meč računaće se kvota jedan.
    """

    static let bejzbolUslovi = """
    KONAČNI ISHOD MEČA
    K1 U meču će pobediti domaćin
    K2 U meču će pobediti gost

    HENDIKEP POENI
    H1 U meču će pobediti domaćin uz uračunat hendikep
    H2 U meču će pobediti gost uz uračunat hendikep

    UKUPNO POENA
    < Ukupan broj poena u mecu biće manji od zadate granice
    > Ukupan broj poena u mecu biće veći od zadate granice

    U slučaju prekida bejzbol meča, bilo kada u toku meča, kada utakmica ne bude nastavljena u roku od 12 sati od prvobitnog termina, sve opklade čiji se ishod realizovao do trenutka prekida proglašavaju se važećim. Sve opklade čiji ishod nije realizovan i mogao je makar teoretski da se promeni, računaće se kvotom jedan.

    U slučaju odlaganja bejzbol meča, ukoliko se utakmica ne odigra u roku od 12 sati od prvobitnog termina, sve opklade su nevažeće i računaće se kvotom jedan.

    U slučaju da isti protivnici igraju dve utakmice istog dana, računa se samo rezultat prve utakmice.

    Ukoliko je u pitanju japanska liga, ukoliko i nakon tri odigrana ekstra ininga ishod utakmice bude nerešen, sve opklade smatraće se nevažećim i računaće se kvotom jedan.
    """

    static let americkiFudbalUslovi = """
    KONAČNI ISHOD MEČA (UKLJUČUJUĆI PRODUŽETKE)
    K1 U meču će pobediti domaćin
    K2 U meču će pobediti gost

    UKUPNO POENA U MEČU (UKLJUČUJUĆI PRODUŽETKE)
    bpk< Ukupan broj poena na meču će biti manji od zadate granice
    bpk> Ukupan broj poena na meču će biti veći od zadate granice

    UKUPAN BROJ POENA U I POLUVREMENU
    bpp< Ukupan broj poena na kraju I poluvremena će biti manji od zadate granice
    bpp> Ukupan broj poena na kraju I poluvremena će biti veći od zadate granice

    KONAČNI ISHOD SA HENDIKEPOM (UKLJUČUJUĆI PRODUŽETKE)
    H1 U meču će pobediti domaćin uz uračunat hendikep
    H2 U meču će pobediti gost uz uračunat hendikep

    KONAČNI ISHOD PRVOG POLUVREMENA SA HENDIKEPOM
    HP1 U poluvremenu će pobediti domaćin uz uračunat hendikep
    HP2 U poluvremenu će pobediti gost uz uračunat hendikep

    Važeći su eventualni produžeci za sve vrste igara.

    U slučaju prekida NFL događaja, bilo kada u toku meča, kada utakmica ne bude nastavljena do kraja narednog dana po lokalnom vremenu, sve opklade čiji se ishod realizovao do trenutka prekida proglašavaju se važećim. Sve opklade čiji ishod nije realizovan i mogao je makar teoretski da se promeni, računaće se kvotom jedan.

    Primer: Na meču Nju Ingland – Pitsburg prekid je usledio tokom treće četvrtine pri rezultatu 14:7 (7:0,7:0) za Nju Ingland. Opklade čiji je ishod završen (na primer – pobednik prvog poluvremena, prvo poluvreme u hendikepu, ukupno poena u prvom poluvremenu...) proglašavaju se važećim. Opklade čiji ishod nije realizovan (na primer – konačan ishod, konačan ishod u hendikepu, ukupan broj poena...) računaće se kvotom jedan.

    U slučaju odlaganja NFL utakmice, meč će se smatrati validnim ukoliko bude odigran do kraja narednog dana po lokalnom vremenu. Nakon isteka ovog roka, za izabrani meč računaće se kvota jedan.
    """

    static let odbojskaUslovi = """
    KONAČNI ISHOD MEČA
    K1 U meču će pobediti domaćin
    K2 U meču će pobediti gost

    PRVI SET
    P1 U prvom setu će pobediti domaćin
    P2 U prvom setu će pobediti gost

    HENDIKEP SETOVI
    H1 U meču će pobediti domaćin uz uračunat hendikep
    H2 U meču će pobediti gost uz uračunat hendikep

    HENDIKEP POENI
    HP1 Više poena u meču osvojiće domaćin uz uračunat hendikep
    HP2 Više poena u meču osvojiće gost uz uračunat hendikep

    POENI PRVI SET
    < Ukupan broj poena u prvom setu biće manji od zadate granice
    > Ukupan broj poena u prvom setu biće veci od zadate granice

    UKUPNO POENA
    < Ukupan broj poena u meču biće manji od zadate granice
    > Ukupan broj poena u meču biće veći od zadate granice

    U slučaju prekida odbojkaškog događaja, bilo kada u toku meča, kada utakmica ne bude nastavljena do kraja narednog dana po lokalnom vremenu, sve opklade čiji se ishod realizovao do trenutka prekida proglašavaju se važećim. Sve opklade čiji ishod nije realizovan i mogao je makar teoretski da se promeni, računaće se kvotom jedan.

    U slučaju odlaganja odbojkaške utakmice, meč će se smatrati validnim ukoliko bude odigran do kraja narednog dana po lokalnom vremenu. Nakon isteka ovog roka, za izabrani meč računaće se kvota jedan.
    """

    static let rukometUslovi = """
    KONAČNI ISHOD MEČA
    K1 U meču će pobediti domaćin
    KX U meču će biti nerešeno
    K2 U meču će pobediti gost

    KONAČNI ISHOD SA HENDIKEPOM
    H1 U meču će pobediti domaćin uz uračunat hendikep
    H2 U meču će pobediti gost uz uračunat hendikep

    UKUPNO GOLOVA U MEČU
    < Ukupan broj golova u meču će biti manji od zadate granice
    > Ukupan broj golova u meču će biti veci od zadate granice

    UKUPNO GOLOVA U I POLUVREMENU
    < Ukupan broj golova u I poluvremenu će biti manji od zadate granice
    > Ukupan broj golova u I poluvremenu će biti veći od zadate granice

    UKUPNO GOLOVA U II POLUVREMENU
    < Ukupan broj golova u II poluvremenu će biti manji od zadate granice
    > Ukupan broj golova u II poluvremenu će biti veći od zadate granice

    Važeći je regularni tok meča, bez produžetaka.

    U slučaju prekida rukometnog događaja, bilo kada u toku meča, kada utakmica ne bude nastavljena do kraja narednog dana po lokalnom vremenu, sve opklade čiji se ishod realizovao do trenutka prekida proglašavaju se važećim. Sve opklade čiji ishod nije realizovan i mogao je makar teoretski da se promeni, računaće se kvotom jedan.

    Primer: Na utakmici Partizan – Crvena zvezda prekid je usledio u 43. minutu pri rezultatu 19:19 (10:9). Opklade čiji je ishod završen (na primer – pobednik prvog poluvremena, ukupno golova u prvom poluvremenu...) proglašavaju se važećim. Opklade čiji ishod nije realizovan (na primer – konačan ishod, konačan ishod u hendikepu, ukupan broj golova...) računaće se kvotom jedan.

    U slučaju odlaganja rukometne utakmice, meč će se smatrati validnim ukoliko bude odigran do kraja narednog dana po lokalnom vremenu. Nakon isteka ovog roka, za izabrani meč računaće se kvota jedan.
    """

    static let vaterpoloUslovi = """
    KONAČNI ISHOD MEČA
    K1 U meču će pobediti domaćin
    KX U meču će biti nerešeno
    K2 U meču će pobediti gost

    KONAČNI ISHOD SA HENDIKEPOM
    H1 U meču će pobediti domaćin uz uračunat hendikep
    H2 U meču će pobediti gost uz uračunat hendikep

    UKUPNO GOLOVA NA MEČU
    < Ukupan broj golova na meču će biti manji od zadate granice
    > Ukupan broj golova na meču će biti veci od zadate granice

    Važeći je regularni tok meča, bez produžetaka.

    U slučaju prekida vaterpolo događaja, bilo kada u toku meča, kada utakmica ne bude nastavljena do kraja narednog dana po lokalnom vremenu, sve opklade čiji se ishod realizovao do trenutka prekida proglašavaju se važećim. Sve opklade čiji ishod nije realizovan i mogao je makar teoretski da se promeni, računaće se kvotom jedan.

    Primer: Na utakmici Radnički - Jug prekid je usledio tokom treće četvrtine pri rezultatu 8:7 (2:2, 4:3). Opklade čiji je ishod završen (na primer – ukupan broj golova veći ako je granica bila 14,5...) proglašavaju se važećim. Opklade čiji ishod nije realizovan (na primer – konačan ishod, konačan ishod u hendikepu...) računaće se kvotom jedan.

    U slučaju odlaganja vaterpolo utakmice, meč će se smatrati validnim ukoliko bude odigran do kraja narednog dana po lokalnom vremenu. Nakon isteka ovog roka, za izabrani meč računaće se kvota jedan.
    """

    static let fudbalSpecijalUslovi = """
    ŽUTI KARTONI:
    Ukupno žutih kartona – Igrač pogađa broj žutih kartona na celoj utakmici, više ili manje u odnosu na zadatu granicu
    Ukupno žutih kartona I poluvreme – Igrač pogađa broj žutih kartona u prvom poluvremenu utakmice, više ili manje u odnosu na zadatu granicu
    Ukupno žutih kartona II poluvreme – Igrač pogađa broj žutih kartona u drugom poluvremenu, više ili manje u odnosu na zadatu granicu
    Prvi žuti karton – Igrač pogađa tim čiji će fudbaler dobiti prvi žuti karton na utakmici
    Vreme prvog žutog kartona – Igrač pogađa vreme kada će biti dosuđen prvi žuti karton na utakmici (naprimer: prvi žuti karton biće od početka meča do 30. minuta ili od 31. minuta do kraja utakmice)

    KORNERI:
    Ukupno kornera - Igrač pogađa broj kornera na celoj utakmici, više ili manje u odnosu na zadatu granicu
    Ukupno kornera I poluvreme - Igrač pogađa broj kornera u prvom poluvremenu utakmice, više ili manje u odnosu na zadatu granicu
    Ukupno kornera II poluvreme - Igrač pogađa broj kornera u drugom poluvremenu utakmice, više ili manje u odnosu na zadatu granicu
    Ukupno kornera domaćin - Igrač pogađa broj kornera koji će izvesti domaćin na celoj utakmici, više ili manje u odnosu na zadatu granicu
    Ukupno kornera gost - Igrač pogađa broj kornera koji će izvesti gost na celoj utakmici, više ili manje u odnosu na zadatu granicu
    Prvi korner – Igrač pogađa koji će tim izvesti prvi korner na utakmici
    Vreme prvog kornera – Igrač pogađa vreme kada će biti izveden prvi korner na utakmici (naprimer: prvi korner će biti izveden od početka meča do 9. minuta ili od 10. minuta do kraja utakmice)
    JEDANAESTERCI:
    Da – U toku utakmice biće dosuđen jedan ili više jedanaesteraca
    Ne – U toku utakmice neće biti dosuđenih jedanaesteraca

    CRVENI KARTONI:
    Da – U toku utakmice biće jedan ili više crvenih kartona
    Ne – U toku utakmice neće biti crvenih kartona

    VAR NA MEČU:
    Da - U toku utakmice glavni sudija će direktno koristiti pomoć VAR tehnologije
    Ne - U toku uktamice glavni sudija neće direktno koristiti pomoć VAR tehnologije
    Napomena:
    Tip Da je dobitan samo ukoliko bilo kad u toku meča sudija samostalno na monitoru odgleda sumnjivu situaciju kako bi doneo konačnu odluku.
    Tip Ne je dobitan ukoliko sudija ni na koji način tokom meča ne iskoristi VAR tehnologiju, kao i u slučaju kada se samo konsultuje sa VAR sudijama putem radio veze,
    a pritom samostalno ne odgleda sumnjivu situaciju.

    STRELAC NA MEČU:
    Dvostruki strelac – Bilo koji od igrača postići će dva ili više golova na utakmici
    Trostruki strelac – Bilo koji od igrača postići će tri ili više golova na utakmici
    Strelac prvog gola – Odabrani igrač postići će prvi pogodak na utakmici
    Strelac jednog ili više golova – Odabrani igrač postići će jedan ili više golova na utakmici
    Strelac dva ili više golova – Odabrani igrač postići će dva ili više golova na utakmici
    Strelac tri ili više golova – Odabrani igrač postići će tri ili više golova na utakmici
    Strelac poslednjeg gola – Odabrani igrač postići će poslednji pogodak na utakmici

    VREME PRVOG GOLA- Igrač pogađa vreme kada će biti postignut prvi pogodak na utakmici, od početka meča do 29. minuta ili od 30. minuta do kraja utakmice
    PRVA IZMENA - Igrač pogađa koji će tim izvršiti prvu izmenu na utakmici
    POČETNI UDARAC - Igrač pogađa koji će tim izvesti početni udarac na utakmici

    Za igru „žuti kartoni“ kod sabiranja žutih kartona računaju se samo žuti kartoni igrača na terenu, ne računaju se rezervni igrači i treneri. Ne računa se drugi žuti karton istom igraču koji automatski prelazi u crveni, računa se samo prvi dobijeni žuti karton.
    Za igru „strelac na meču“ ne računaju se autogolovi.
    Za igru „vreme prvog gola“ ukoliko se utakmica završi rezultatom 0:0 tiketi se računaju kao nedobitni.
    U slučaju da fudbaler na kojeg se igrač kladio ne nastupi na meču (za igre strelac prvog i poslednjeg pogotka, strelac jednog, dva, tri ili više golova), računaće se kvota 1.
    Za igru „prvi žuti karton“, ukoliko dva igrača istovremeno dobiju žuti karton, računaće se kvota 1.
    Za igru „prvi žuti karton“, ukoliko na utakmici ne bude žutih kartona, tiketi se računaju kao nedobitni.
    Za igru „vreme prvog žutog kartona“, ukoliko na utakmici ne bude žutih kartona, tiketi se računaju kao nedobitni. Za igru „prvi korner“, ukoliko na utakmici ne bude kornera, tiketi se računaju kao nedobitni.
    Za igru „vreme prvog kornera“, ukoliko na utakmici ne bude kornera, tiketi se računaju kao nedobitni.
    Za igru „prva izmena“, ukoliko istovremeno dođe do dvostruke izmene, računaće se kvota 1.
    Za igru „prva izmena“, ukoliko na utakmici ne bude izmena , tiketi se računaju kao nedobitni.
    U igri „Penal i Crveni karton“,tiket će se smatrati dobitnim ukoliko se oba događaja dogode na meču (nije neophodno da se oba događaja dese u isto vreme).
    Igru „Penal i Crveni karton“ nije moguce vezati sa igrama: „Penal“, „Crveni karton“, „žuti kartoni“(ceo sektor igara), „Postiže gol“(ceo sektor igara), i „Vreme 1. gola“.
    U igri „Zamena postiže gol“, ukoliko izmena postigne autogol, tiket će se smatrati nedobitnim.

    Sve igre iz specijala mogu se vezivati sa svim mečevima iz liste i dopune. Igre iz specijala za jedan meč ne mogu se vezivati međusobno, ali se mogu vezivati sa igrama iz specijala za druge mečeve. Za sve igre iz specijala važeći je regularni tok meča (90 minuta plus sudijska nadoknada).
    Za sve igre iz specijala marodavni su zvanični sajtovi : www.fifa.com , www.uefa.com , www.laliga.es, www.premierleague.com , www.bundesliga.com , www.ligue1.com , www.legaseriea.it.

    
    """

    static let kosarkaSpecijalUslovi = """
    BROJ POENA – Igrač pogađa koliko će poena postići određeni košarkaš na utakmici, više ili manje u odnosu na zadatu granicu
    BROJ SKOKOVA - Igrač pogađa koliko će skokova ostvariti određeni košarkaš na utakmici, više ili manje u odnosu na zadatu granicu
    BROJ ASISTENCIJA - Igrač pogađa koliko će asistencija ostvariti određeni košarkaš na utakmici, više ili manje u odnosu na zadatu granicu
    DUELI IGRAČA – Igrač pogađa koji će od dva ponuđena košarkaša postići više poena na utakmici
    DUBL POENI IGRAČA – Igrač pogađa koji će od dva ponuđena dubla postići više poena na utakmici
    PRVI SKOK NA MEČU – Igrač pogađa da li će domaćin ili gost ostvariti prvi skok na meču.
    PRVI FAUL NA MEČU – Igrač pogađa da li će domaćin ili gost napraviti prvi faul na meču.

    U slučaju da igrač ne nastupi na meču računaće se kvota jedan.
    Za igru „dubl poeni igrača“, ukoliko jedan od dva ponuđena igrača iz datog dubla ne zabeleži nastup na meču, važi kvota 1 za dati dubl.
    Za igru „dueli igrača“, u slučaju da oba igrača postignu isti broj poena, računaće se kvota jedan.
    Za igru „dubl poeni igrača“, u slučaju da oba dubla postignu isti broj poena, računaće se kvota jedan.
    Igre „broj poena/skokova/asistencija“i „dueli igrača“ mogu se vezivati sa svim mečevima iz liste i dopune.
    Igre „broj poena/skokova/asistencija“i „dueli igrača“ mogu se vezivati sa svim mečevima iz liste i dopune.
    Igra „dubl poeni igrača“ može se vezivati sa svim mečevima iz liste i dopune, osim igre „igrač postiže poena na meču“ i to samo u slučaju da se ponavlja isti igrač kao u datom dublu.
    Isti igrač se ne može vezivati u duelu i broju poena/skokova/asistencija.
    Produžeci se računaju za sve vrste igara u specijalu.

    NAPOMENA: Kladionica eventualne promene na zvaničnim sajtovima kod broja poena, skokova i asistencija prihvata kao merodavne samo do 24h posle završenog meča, nakon toga nece prihvatati nikakve ispravke.
    """

    static let futsalUslovi = """
    KONAČAN ISHOD MEČA
    1 U meču će pobediti domaćin
    X U meču će biti nerešeno
    2 U meču će pobediti gost


    UKUPNO GOLOVA
    < Ukupan broj golova na meču biće manji od zadate granice
    > Ukupan broj golova na mecu biće veći od zadate granice

    U slučaju prekida futsal meča, bilo kada u toku meča, kada utakmica ne bude nastavljena do kraja narednog dana po lokalnom vremenu, kvota jedan biće obračunata za sve vrste igara.

    Sve futsal vrste igara se baziraju na regularnom vremenu, produžeci i penali se ne računaju.
    """

    static let formulaUslovi = """
    POL POZICIJA
    1 Vozač će pobediti na poslednjem kvalifikacionom treningu


    POBEDNIK
    1 Vozač će pobediti na trci


    PLASMAN 1-3. MESTO
    1 Vozač će biti među trojicom prvoplasiranih na trci


    PLASMAN 1-10. MESTO
    1 Vozač će završiti trku među prvih deset, odnosno osvojiće barem jedan poen

    U slučaju da se odabrani vozač ne pojavi na poslednjem kvalifikacionom treningu ili na glavnoj trci, sve opklade vezane za tog takmičara jesu nevažeće i računaće se kvotom jedan.

    U slučaju da odabrani vozač započne kvalifikacije ili glavnu trku i odustane u toku trke, sve opklade vezane za tog takmičara su važeće.

    U slučaju odlaganja trke formule 1, trka će se smatrati validnom ukoliko bude održana u roku od 48 sati od najavljenog termina. Nakon isteka ovog vremena, za odabranu trku računaće se kvota jedan.

    Kao konačan rezultat trke računaće se redosled prolaska takmičara kroz cilj, dok se sve eventualne naknadne promene (kažnjavanja, diskvalifikacije) neće uzimati u obzir.
    """

    static let snookerUslovi = """
    KONAČAN ISHOD
    1 U meču će pobediti domaćin
    2 U meču će pobediti gost


    KONAČAN ISHOD SA HENDIKEPOM
    H1 U meču će pobediti domaćin uz uračunat hendikep
    H2 U meču će pobediti gost uz uračunat hendikep


    UKUPNO FREJMOVA U MEČU
    bfk< Ukupan broj frejmova na meču će biti manji od zadate granice
    bfk> Ukupan broj frejmova na meču će biti veći od zadate granice

    U slučaju da jedan od igrača bude proglašen za pobednika pre nego što je pun broj frejmova odigran, opklade će biti važeće pod uslovom da je meč počeo, u suprotnom će se računati kvota jedan za sve vrste igara.

    Ukoliko je meč odložen ili prekinut, sve opklade biće važeće do završetka meča.
    """

    static let uzivoKladjenjeUslovi = """
    Fudbal
    - U slučaju prekida fudbalskog meča sve opklade koje do tog trenutka nisu završene, a ukoliko se meč ne nastavi u naredna 24 sata, računaće se kvotom jedan!

    - Sve opklade koje su realizovane do tog trenutka računaju se kao dobitne ili gubitne, zavisno od tipa igre i rezultata, do prekida meča.
    - Konačan rezultat jeste rezultat posle završenog regularnog toka meča, uključujući i sudijsku nadoknadu vremena. Dakle, bez eventualnih produžetaka.
    Primer:
    - Na utakmici Barselona – Real Madrid prvo poluvreme završeno je rezultatom 2:0. Prekid je usledio u 68. minutu pri rezultatu 3:0. Opklade čiji je ishod završen (na primer: prvo poluvreme, 3+, 0-2, tim 1 daje dva gola...) proglašavaju se važećim. Opklade čiji ishod nije realizovan (na primer: konačan ishod, 4+, hendikep...) računaće se kvotom jedan ukoliko se meč ne nastavi u naredna 24 sata.

    Broj kartona na utakmici
    - Žuti karton se racuna kao jedan (karton), crveni karton se racuna kao dva.
    - Drugi žuti karton koji rezultuje crvenim kartonom se zanemaruje.
    - Maksimalan broj kartona za jednog igraca je tri.

    - Za broj kartona iskljucivo se uzima u obzir regularan tok utakmice, odnosno 90 minuta igre i predvidena nadoknada vremena od strane sudije.
    - Žuti kartoni koji se dodele po završetku utakmice ili u produžetku ne uzimaju se u obzir.

    - Kartoni koje sudija dodeli treneru, strucnom štabu, igracu na klupi ili vec zamenjenom igracu ne uzimaju se u obzir.
    
    - U slucaju prekida utakmice pre njenog regularnog završetka, svi aktivni marketi obraduju se kao P (kvota 1.00), dok marketi ciji je ishod vec poznat ostaju regularno obradeni.

    Primer:
    Utakmica je prekinuta u 70. minutu, a broj kartona do tog trenutka je 6. Granice 1.5, 2.5, 3.5, 4.5, 5.5 bice obradene kao over, dok ce granice 6.5 i 7.5 biti obradene kao P (kvota 1.00). Korneri • Korneri koji su dosudjeni, a nisu izvedeni ne uzimaju se u obzir. • Korneri koji su dosudjeni u produžetku ne uzimaju se u obzir.

    Košarka

    - U slučaju prekida košarkaškog meča sve opklade koje do tog trenutka nisu završene, a ukoliko se meč ne nastavi u naredna 24 sata, računaće se kvotom jedan! - Za igre, pobednik četvrtine i pobednik poluvremena, postoji nerešen ishod.
    - Sve opklade koje su realizovane do trenutka prekida (ishod je već poznat), biće proglašene kao dobitne ili gubitne, zavisno od tipa igre i rezultata, do prekida meča.
    - Konačan rezultat jeste rezultat postignut posle završenog regularnog toka meča ili posle završenih produžetaka, ukoliko je regularni tok završen nerešenim rezultatom. Eventualno odigravanje produžetaka biće važeće ukoliko je u nazivu igre tako naznačeno (FT+OT).
    Primer:
    - Na utakmici Barselona – Real Madrid prvo poluvreme završeno je rezultatom 45:45. Prekid je usledio u 35. minutu pri rezultatu 87:85 za Barselonu. Opklade čiji je ishod završen (na primer: prvo poluvreme, ukupno poena prvo poluvreme, pobednik prve, druge ili treće četvrtine...) proglašavaju se važećim. Opklade čiji ishod nije realizovan (na primer: konačan ishod, konačan ishod u hendikepu, ukupno poena, pobednik četvrte četvrtine...) računaće se kvotom jedan ukoliko se meč ne nastavi u narednih 24 sata.
    Napomena:
    - U slučaju odigravanja produžetaka, a da regularni tok meča nije završen nerešenim rezultatom (primer: takmičenja sa revanš mečevima), produžetci se ne važe.

    Tenis:
    - U slučaju prekida teniskog meča sve opklade koje do tog trenutka nisu završene, a ukoliko se meč ne nastavi u narednih 48 sati, računaće se kvotom jedan!
    - Sve opklade koje su realizovane do tog trenutka (ishod već poznat), računaju se kao dobitne ili gubitne, zavisno od tipa igre i rezultata do prekida meča.
    Primer:
    - Na meču Đoković – Nadal prekid je usledio pri rezultatu 6:3, 4:2 za Đokovića. Opklade čiji je ishod završen (na primer: pobednik prvog seta, ukupno gemova u prvom setu...) proglašavaju se važećim. Opklade čiji ishod nije realizovan (na primer: konačan ishod, ukupan broj gemova na meču...) računaće se kvotom jedan ukoliko se meč ne nastavi u narednih 48 sati.

    Hokej:
    - U slučaju prekida hokejaškog meča sve opklade koje do tog trenutka nisu završene, a ukoliko se meč ne nastavi u naredna 24 sata, računaće se kvotom jedan!
    - Sve opklade koje su realizovane do tog trenutka, računaju se kao dobitne ili gubitne, zavisno od tipa igre i rezultata do prekida meča.
    - Konačan rezultat jeste rezultat posle završenog regularnog toka meča. Dakle, bez eventualnih produžetaka.

    Odbojka:
    - U slučaju prekida odbojkaškog meča sve opklade koje do tog trenutka nisu završene, a ukoliko se meč ne nastavi u naredna 24 sata, računaće se kvotom jedan!
    - Sve opklade koje su realizovane do tog trenutka, računaju se kao dobitne ili gubitne, zavisno od tipa igre i rezultata do prekida meča.

    Rukomet:
    - Za sve ponuđene igre važeć je regularan tok meča (60 minuta), osim ako nije naglašeno drugačije.
    - U slučaju prekida rukometnog meča sve opklade čiji ishod do trenutka prekida meča nije realizovan, a meč se u roku od 48 ne nastavi, računaće se kvotom 1.
    - Sve opklade koje su realizovane do trenutka prekida meča, smatraju se važećim i računaju se kao dobitne ili gubitne, zavisno od tipa igre i rezultata do prekida meča.


    Američki fudbal: - Pravila za obradu prekinutih mečeva
    - Ako se započeti meč prekine bilo kada pre regularnog završetka meča i ne završi do kraja u roku od 48 sati od vremena početka meča sve igre čiji ishod do momenta prekida nije poznat biće obradjene kao (kvota 1).
    - Sve igre čiji je ishod poznat do momenta prekida i gde se sama odigrana igra završila rezultat je važeći i odigrani tipovi će biti obradjeni.
    - U slučaju prekida meča zbog određenih događaja na meču (npr. napuštanje meča nekog od timova) i rezultovanja meča službenim rezultatom, službeni rezultat se neće uzimati u obzir već će meč biti obrađen po pravilima za prekinute mečeve a po rezultatu koji je bio u trenutku prekida meča.

    MOLIMO IGRAČE DA KOD KLAĐENJA UŽIVO PROVERE TIKETE, ODNOSNO DA BLAGAJNIKU DAJU PRECIZNU INFORMACIJU ŠTA ŽELE DA IGRAJU, JER STORNIRANJE TIKETA NA KLAĐENJU UŽIVO NIJE MOGUĆE.
    """

    static let ragbiUslovi = """
    Rezultat ragbi meča se uvek bazira na 80 minuta igre, ukoliko nije drugačije naglašeno.

    1X2 (KONAČAN ISHOD)
    Tri moguća ishoda:
    1 U regularnom toku meču će pobediti domaćin
    X U regularnom toku meč će se zavrsiti nerešeno
    2 U regularnom toku meča će pobediti gost


    DC (DUPLA ŠANSA)
    Tri moguća ishoda:
    DS1X Domaćin pobedjuje na utakmici ili na utakmici nema pobednika
    DS12 Domaćin pobedjuje na utakmici ili gost pobedjuje na utakmici
    DSX2 Na utakmici nema pobednika ili gost pobedjuje na utakmici


    Handicap
    H1 U meču će pobediti domaćin uz uračunat hendikep
    H2 U meču će pobediti gost uz uračunat hendikep


    *U slučaju eventualnog prekida meča, ukoliko meč ne bude nastavljen do kraja sledećeg dana po lokalnom vremenu, važi kvota 1,00 za sve vrste igara. U tom slučaju eventualni ponovljeni meč neće biti uziman u obzir.
    """

    static let goloviULigiUslovi = """
    UKUPNO GOLOVA
    < Ukupan broj golova odredjene lige biće manji od zadate granice
    > Ukupan broj golova odredjene lige biće veći od zadate granice

    Ukoliko je jedan (1) meč iz lige odložen računaće se kao daje na tom meču palo tačno dva (2) gola.

    Ukoliko su dva (2) ili više mečeva odloženi kladionica će ligu proglasiti nevažećom i računaće se kvota jedan (1).

    *Ukoliko je meč prekinut u prvom poluvremenu računaće se kao da je na tom meču palo tačno dva (2) gola.

    *Ukoliko je meč prekinut po završetku prvog poluvremena računaće se broj golova postignutih do trenutka prekida.
    """
}
